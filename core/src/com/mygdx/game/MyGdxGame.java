package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class MyGdxGame extends ApplicationAdapter implements InputProcessor {
	SpriteBatch batch;
	ParticleEffect particleEffect;
	Texture img;
	OrthographicCamera camera;
	TiledMap tiledMap;
	TiledMapRenderer tiledMapRenderer;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		batch.setColor(Color.BLACK);
		float width = Gdx.graphics.getWidth();
		float height = Gdx.graphics.getHeight();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, width, height);
		camera.update();
		tiledMap = new TmxMapLoader().load("Mapa.tmx");
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
		particleEffect = new ParticleEffect();
		particleEffect.load(Gdx.files.internal("Explosion.p"), Gdx.files.internal(""));
		particleEffect.setPosition(width/2, height/2);
		particleEffect.start();
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		particleEffect.update(Gdx.graphics.getDeltaTime());
		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();
		particleEffect.setPosition(Gdx.input.getX(), -Gdx.input.getY()+ Gdx.graphics.getHeight());
		batch.begin();
		particleEffect.draw(batch);
		batch.end();
		if(particleEffect.isComplete()) particleEffect.reset();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Input.Keys.LEFT)

			camera.translate(-32,0);

		if(keycode == Input.Keys.RIGHT)

			camera.translate(32,0);

		if(keycode == Input.Keys.UP)

			camera.translate(0,-32);

		if(keycode == Input.Keys.DOWN)

			camera.translate(0,32);

		if(keycode == Input.Keys.NUM_1)

			tiledMap.getLayers().get(0).setVisible(

					!tiledMap.getLayers().get(0).isVisible());

		if(keycode == Input.Keys.NUM_2)

			tiledMap.getLayers().get(1).setVisible(

					!tiledMap.getLayers().get(1).isVisible());
		if(keycode == Input.Keys.NUM_3)

			tiledMap.getLayers().get(2).setVisible(

					!tiledMap.getLayers().get(2).isVisible());

		if(keycode == Input.Keys.NUM_4)

			tiledMap.getLayers().get(3).setVisible(

					!tiledMap.getLayers().get(3).isVisible());
		if(keycode == Input.Keys.NUM_5)

			tiledMap.getLayers().get(4).setVisible(

					!tiledMap.getLayers().get(4).isVisible());

		if(keycode == Input.Keys.NUM_6)

			tiledMap.getLayers().get(5).setVisible(

					!tiledMap.getLayers().get(5).isVisible());
		if(keycode == Input.Keys.Q)
			particleEffect.scaleEffect(2);
		if(keycode == Input.Keys.W)
			particleEffect.scaleEffect(0.5f);

		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
